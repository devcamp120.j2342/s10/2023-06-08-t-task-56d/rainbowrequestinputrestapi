package com.devcamp.api.rainbowinputapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RainbowInputApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RainbowInputApiApplication.class, args);
	}

}
