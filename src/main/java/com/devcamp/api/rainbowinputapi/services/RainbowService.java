package com.devcamp.api.rainbowinputapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class RainbowService {
    private String[] rainbows = {"red", "orange", "yellow", "green", "blue", "indigo", "violet"};

    public ArrayList<String> searchRainbows(String keyword) {
        ArrayList<String> lstRainbows = new ArrayList<>();

        for (String rainbow : this.rainbows) {
            if (rainbow.contains(keyword)) {
                lstRainbows.add(rainbow);
            }
        }
        return lstRainbows;
    }

    public String getRainbow(int index) {
        String rainbow = "";

        if (index >= 0 && index <= 6) {
            rainbow = this.rainbows[index];
        }
        
        return rainbow;
    }
}
